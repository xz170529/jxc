package com.atguigu.jxc.controller.jichuModel;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * time:  2023/9/11
 */
@RestController
@RequestMapping
public class GysController {

    @Autowired
    GysService gysService;

    /**
     * 供应商列表
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/supplier/list")
    public Map<String,Object> gyslist
            (@RequestParam(value = "page",required = false) Integer page
            ,@RequestParam(value ="rows",required = false) Integer rows
            ,@RequestParam(value ="supplierName",required = false) String supplierName){
        Map<String,Object> map = gysService.getGysList(page,rows,supplierName);
        return map;
    }

    /**
     * 供应商修改增加
     * @param supplier
     * @return
     */
    @PostMapping("/supplier/save")
    public ServiceVO save(
            Supplier supplier
                          ){
        gysService.save(supplier);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,"请求成功",null);
    }

    /**
     * 删除供应商（批量）
     * @param ids
     * @return
     */
    @PostMapping("/supplier/delete")
    public ServiceVO delete(
            @RequestParam("ids") String  ids
    ){
        gysService.delete(ids);
        System.out.println("dadadadada");
        return new ServiceVO(SuccessCode.SUCCESS_CODE,"请求成功",null);
    }
}
