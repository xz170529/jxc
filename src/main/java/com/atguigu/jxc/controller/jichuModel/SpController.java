package com.atguigu.jxc.controller.jichuModel;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.SpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * time:  2023/9/11
 */
@RestController
@RequestMapping
public class SpController {
    @Autowired
    SpService spService;

    /**
     * 查询单位列表
     * @return
     */
    @PostMapping("/unit/list")
    public Map<String,Object> getUnitList(){
        Map<String,Object> map = spService.getUnitList();
        return map;
    }

    /**
     * 查询所有商品信息
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @PostMapping("/goods/list")
    public Map<String,Object> getGoodsList(@RequestParam("page") Integer page,
                                          @RequestParam("rows") Integer rows,
                                          @RequestParam(value = "goodsName",required = false) String goodsName,
                                          @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId){
        Map<String,Object> map = spService.getGoodsList(page,rows,goodsName,goodsTypeId);
        return map;
    }

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @PostMapping("/goodsType/save")
    public ServiceVO saveGoodsType(@RequestParam("goodsTypeName") String  goodsTypeName,
                                   @RequestParam("pId") Integer  pId
               ){
        spService.saveGoodsType(goodsTypeName,pId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @PostMapping("/goodsType/delete")
    public ServiceVO deleteGoodsType(@RequestParam("goodsTypeId") Integer  goodsTypeId
    ){
        Integer i = spService.deleteGoodsType(goodsTypeId);
        if(i==0){
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS,null);
        }else {
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
        }

    }

    /**
     * 修改新增商品
     * @param goods
     * @return
     */
    @PostMapping("/goods/save")
    public ServiceVO saveGoods(Goods goods){
        spService.saveAndUpdateGoods(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     * @param goodsId
     * @return
     */
    @PostMapping("/goods/delete")
    public ServiceVO deleteGoods(@RequestParam("goodsId") Integer goodsId){
        Integer i = spService.deleteGoods(goodsId);
        if(i==0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
        }
        if(i==1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS,null);
        }else{
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS,null);
        }

    }

}
