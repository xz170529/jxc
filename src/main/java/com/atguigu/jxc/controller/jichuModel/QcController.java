package com.atguigu.jxc.controller.jichuModel;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.service.QcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * time:  2023/9/11
 */
@RestController
@RequestMapping
public class QcController {
    @Autowired
    QcService qcService;

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam("page") Integer page,
                                                     @RequestParam("rows")Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false)String nameOrCode){
        Map<String,Object> map = qcService.getNoInventoryQuantity(page,rows,nameOrCode);
        return map;
    }
    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @PostMapping("/goods/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(@RequestParam("page") Integer page,
                                                     @RequestParam("rows")Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false)String nameOrCode){
        Map<String,Object> map = qcService.getHasInventoryQuantity(page,rows,nameOrCode);
        return map;
    }
    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @PostMapping("/goods/saveStock")
    public ServiceVO saveStock(@RequestParam("goodsId") Integer goodsId,
                               @RequestParam(value = "inventoryQuantity",required = false)Integer inventoryQuantity,
                               @RequestParam(value = "purchasingPrice",required = false)double purchasingPrice){
        qcService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    @PostMapping("/goods/deleteStock")
    public ServiceVO deleteStock(@RequestParam("goodsId") Integer goodsId){
        Integer i = qcService.deleteStock(goodsId);
        if(i==0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
        }
        if(i==1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS,null);
        }else{
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS,null);
        }
    }
}
