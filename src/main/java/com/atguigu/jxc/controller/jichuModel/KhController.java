package com.atguigu.jxc.controller.jichuModel;

import com.atguigu.jxc.dao.KhDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.KhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * time:  2023/9/11
 */
@RestController
@RequestMapping
public class KhController {
    @Autowired
    KhService khService;

    /**
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/customer/list")
    public Map<String,Object> getKhList(
            @RequestParam("page") Integer page,
            @RequestParam("rows") Integer rows,
            @RequestParam(value = "customerName",required = false) String  customerName
    ){
        Map<String,Object> map = khService.getKhList(page,rows,customerName);
        return map;
    }

    /**
     * 添加修改
     * @param customer
     * @return
     */
    @PostMapping("/customer/save")
    public ServiceVO save(Customer customer){
        khService.save(customer);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    @PostMapping("/customer/delete")
    public ServiceVO delete(@RequestParam("ids") String  ids){
        khService.delete(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
}
