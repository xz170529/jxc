package com.atguigu.jxc.controller.jichuModel;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.KcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * time:  2023/9/11
 */
@RestController
@RequestMapping
public class KcController {
    @Autowired
    KcService kcService;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/damageListGoods/save")
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr, HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        kcService.saveDamageListGoods(damageList,damageListGoodsStr,user);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    /**
     * 保存报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/overflowListGoods/save")
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr,HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        kcService.saveOverflowListGoods(overflowList,overflowListGoodsStr,user);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    /**
     * 库存报警
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @PostMapping("/goods/listAlarm")
    public Map<String,Object> listAlarm(){
        Map<String,Object> map = kcService.listAlarm();
        return map;
    }
    /**
     * 报损查询
     * @return
     */
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> damageList(String  sTime,String  eTime){
        Map<String,Object> map = kcService.damageList(sTime,eTime);
        return map;
    }
    /**
     * 报溢单查询
     * @return
     */
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> overflowList(String  sTime,String  eTime){
        Map<String,Object> map = kcService.overflowList(sTime,eTime);
        return map;
    }
    /**
     * 报溢单商品信息
     * @return
     */
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> overflowListGoods(Integer overflowListId){
        Map<String,Object> map = kcService.overflowListGoods(overflowListId);
        return map;
    }
    /**
     * 查询报损单商品信息
     * @return
     */
    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> damageListGoods(Integer damageListId){
        Map<String,Object> map = kcService.damageListGoods(damageListId);
        return map;
    }
}
