package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * time:  2023/9/11
 */
@Mapper
public interface GysDao {
    List<Supplier> getGysList(@Param("supplierName") String supplierName);

    void save(@Param("supplier")Supplier supplier);

    void update(@Param("supplier")Supplier supplier);

    void delete(@Param("id")String id);
}
