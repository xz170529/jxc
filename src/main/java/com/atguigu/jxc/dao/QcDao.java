package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * time:  2023/9/11
 */
@Mapper
public interface QcDao {
    List<Goods> getNoInventoryQuantity(@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("nameOrCode")String nameOrCode);

    void update(@Param("goodsId") Integer goodsId,
                @Param("inventoryQuantity") Integer inventoryQuantity,
                @Param("purchasingPrice") double purchasingPrice);

    void deleteStock(@Param("goodsId") Integer goodsId);
}
