package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
@Mapper
public interface GoodsDao {


    String getMaxCode();


    List<Goods> getGoodsList(@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    String getGt(Integer gTypeId);

    Integer getst(Integer goodsId);
}
