package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * time:  2023/9/11
 */
@Mapper
public interface KcDao {
    void saveDamageList(DamageList damageList);

    void saveDamageListGoods(DamageListGoods item);

    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods item);

    List<Goods> listAlarm();

    List<DamageList> damageList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<OverflowList> overflowList(String sTime, String eTime);

    List<DamageListGoods> damageListGoods(@Param("damageListId") Integer damageListId);

    List<OverflowListGoods> overflowListGoods(@Param("overflowListId")Integer overflowListId);
}
