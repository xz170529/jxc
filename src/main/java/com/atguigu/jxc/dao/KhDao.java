package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * time:  2023/9/11
 */
@Mapper
public interface KhDao {
    List<Customer> getKhList(@Param("customerName") String customerName);

    void save(Customer customer);

    void update(Customer customer);

    void delete(@Param("id") String id);
}
