package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * time:  2023/9/11
 */
@Mapper
public interface SpDao {


    List<Unit> getUnitList();

    List<Goods> getGoodsList(@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    void saveGoodsType(@Param("goodsTypeName") String goodsTypeName,@Param("pId") Integer pId);

    void deleteGoodsType(@Param("goodsTypeId")Integer goodsTypeId);

    void save(Goods goods);

    void update(Goods goods);

    Goods selectOne(@Param("goodsId") Integer goodsId);

    void deleteGoods(@Param("goodsId") Integer goodsId);

    void updateParentStatus(@Param("typeId")Integer typeId);

    List<Integer> findChildId(@Param("parentId")Integer parentId);

    void updateParentStatusFalse(Integer item);
}
