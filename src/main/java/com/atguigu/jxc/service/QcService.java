package com.atguigu.jxc.service;

import java.util.Map;

/**
 * time:  2023/9/11
 */
public interface QcService {
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    Integer deleteStock(Integer goodsId);
}
