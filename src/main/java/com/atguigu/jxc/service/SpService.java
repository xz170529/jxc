package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Goods;

import java.util.Map;

/**
 * time:  2023/9/11
 */
public interface SpService {
    Map<String, Object> getUnitList();

    Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveGoodsType(String goodsTypeName, Integer pId);

    Integer deleteGoodsType(Integer goodsTypeId);

    void saveAndUpdateGoods(Goods goods);

    Integer deleteGoods(Integer goodsId);
}
