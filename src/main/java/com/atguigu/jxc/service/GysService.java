package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * time:  2023/9/11
 */

public interface GysService {
    Map<String, Object> getGysList(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier);

    void delete(String ids);
}
