package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SpDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.KhService;
import com.atguigu.jxc.service.SpService;
import com.atguigu.jxc.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * time:  2023/9/11
 */
@Service
public class SpServiceImpl implements SpService {
    @Autowired
    SpDao spDao;
    @Autowired
    GoodsDao goodsDao;
    @Override
    public Map<String, Object> getUnitList() {
        Map<String, Object> map = new HashMap<>();
        List<Unit> list =  spDao.getUnitList();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        if(goodsTypeId != null && goodsTypeId == 1){
            goodsTypeId = null;
        }
        List<Goods> list = spDao.getGoodsList(goodsName,goodsTypeId);
        for (Goods goods : list) {
            Integer typeId = goods.getGoodsTypeId();
            String gt = goodsDao.getGt(typeId);
            goods.setGoodsTypeName(gt);
        }
        int total = list.size();
        List<Goods> pageList = PageUtil.getPageList(list, page, rows);
        map.put("total",total);
        map.put("rows",pageList);
        return map;
    }

    @Override
    public void saveGoodsType(String goodsTypeName, Integer pId) {
        spDao.saveGoodsType(goodsTypeName,pId);
        //getAllGoodsType(-1);
    }
    //递归查询所有节点是否有子节点，无子节点的节点修改状态为0；默认从-1根节点开始
    public List<Integer> getAllGoodsType(Integer parentId){
//        List<Integer> list = new ArrayList<>();
        //获取以该节点为父节点的子节点id集合
        List<Integer> list = findChildId(parentId);
        if(list==null||list.size()==0){
            return null;
        }else {
            list.stream().forEach(item-> {
                List<Integer> integerList = getAllGoodsType(item);
                if(integerList==null){
                    spDao.updateParentStatusFalse(item);
                }
                if(integerList!=null){
                    spDao.updateParentStatus(item);
                }
            });
            return list;
        }
    }

    private List<Integer> findChildId(Integer parentId) {
        List<Integer> list = spDao.findChildId(parentId);
        return list;
    }

    @Override
    public Integer deleteGoodsType(Integer goodsTypeId) {
        //该分类下有商品不允许删除
        List<Goods> goodsList = spDao.getGoodsList(null, goodsTypeId);
        if(goodsList==null || goodsList.size()==0){
            spDao.deleteGoodsType(goodsTypeId);
            return 1;
        }
        return 0;
        //getAllGoodsType(-1);
    }

    @Override
    public void saveAndUpdateGoods(Goods goods) {
        if(goods.getGoodsId() == null){
            spDao.save(goods);
        }else {
            spDao.update(goods);
        }
    }

    @Override
    public Integer deleteGoods(Integer goodsId) {
        Goods goods = spDao.selectOne(goodsId);
        if(goods.getState() == 0){
            spDao.deleteGoods(goodsId);
            return 0;
        }
        if(goods.getState() == 1){
            return 1;
        }else {
            return 2;
        }

    }
}
