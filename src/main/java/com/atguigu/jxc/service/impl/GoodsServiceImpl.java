package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.atguigu.jxc.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getlListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> list = goodsDao.getGoodsList(codeOrName,goodsTypeId);
        for (Goods goods : list) {
            Integer gTypeId = goods.getGoodsTypeId();
            String goodsTypeName =  goodsDao.getGt(gTypeId);
            Integer goodsId = goods.getGoodsId();
            Integer saleTotal = goodsDao.getst(goodsId);
            if(saleTotal == null){
                saleTotal = 0;
            }
            goods.setGoodsTypeName(goodsTypeName);
            goods.setSaleTotal(saleTotal);
        }
        int total = list.size();
//        int pageAll = 0;
//        if(total%rows == 0 ){
//            pageAll = total/rows;
//        }else {
//            pageAll = total/rows+1;
//        }
//        ArrayList<Object> pageList = new ArrayList<>();
//
//        //分页list开始位置
//        int start = (page - 1) * rows;
//        //分页list结束位置
//        int end = 0;
//        //判断是否是最后一页
//        if(page*rows>total){
//            end = total;
//        }else {
//            end = start+30;
//        }
//        //实现分页数据
//        for (int i = start; i < end; i++) {
//            pageList.add(list.get(i));
//        }

        List<Goods> pageList = PageUtil.getPageList(list, page, rows);
        map.put("total",total);
            map.put("rows",pageList);
        return map;
    }


}
