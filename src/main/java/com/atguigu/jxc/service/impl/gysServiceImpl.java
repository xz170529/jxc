package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GysDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GysService;
import com.atguigu.jxc.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * time:  2023/9/11
 */
@Service
public class gysServiceImpl implements GysService {
    @Autowired
    GysDao gysDao;
    @Override
    public Map<String, Object> getGysList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        List<Supplier> list = gysDao.getGysList(supplierName);
        int total = list.size();
        //分页
//        int total = list.size();
//        int pageAll = 0;
//        if(total%rows == 0 ){
//            pageAll = total/rows;
//        }else {
//            pageAll = total/rows+1;
//        }
//        ArrayList<Object> pageList = new ArrayList<>();
//
//        //分页list开始位置
//        int start = (page - 1) * rows;
//        //分页list结束位置
//        int end = 0;
//        //判断是否是最后一页
//        if(page*rows>total){
//            end = total;
//        }else {
//            end = start+30;
//        }
//        //实现分页数据
//        for (int i = start; i < end; i++) {
//            pageList.add(list.get(i));
//        }
        List<Supplier> pageList = PageUtil.getPageList(list,page,rows);
        map.put("total",total);
        map.put("rows",pageList);
        return map;
    }

    @Override
    public void save(Supplier supplier) {
        //判断添加还是修改
        if(supplier.getSupplierId() == null){
            //添加
            gysDao.save(supplier);
        }else{
            //修改
            gysDao.update(supplier);
        }
    }

    @Override
    public void delete(String ids) {
        String[] split = ids.split(",");
        for (String s : split) {
            gysDao.delete(s);
        }
    }
}
