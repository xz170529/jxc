package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.KhDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.KhService;
import com.atguigu.jxc.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * time:  2023/9/11
 */
@Service
public class KhServiceImpl implements KhService {
    @Autowired
    KhDao khDao;
    @Override
    public Map<String, Object> getKhList(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> map = new HashMap<>();
        List<Customer> list =  khDao.getKhList(customerName);
        int total = list.size();
        List<Customer> pageList = PageUtil.getPageList(list, page, rows);
        map.put("total",total);
        map.put("rows",pageList);
        return map;
    }

    @Override
    public void save(Customer customer) {
        if(customer.getCustomerId()==null){
            khDao.save(customer);
        }else {
            khDao.update(customer);
        }
    }

    @Override
    public void delete(String ids) {
        String[] split = ids.split(",");
        for (String s : split) {
            khDao.delete(s);
        }
    }
}
