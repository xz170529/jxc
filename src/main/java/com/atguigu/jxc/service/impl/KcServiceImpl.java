package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.KcDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.KcService;
import com.atguigu.jxc.util.JSONs;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * time:  2023/9/11
 */
@Service
public class KcServiceImpl implements KcService {
    @Autowired
    KcDao kcDao;
    @Override
    public void saveDamageListGoods(DamageList damageList, String damageListGoodsStr,User user) {
        damageList.setUserId(user.getUserId());
        damageList.setTrueName(user.getTrueName());
        kcDao.saveDamageList(damageList);
        Integer damageListId = damageList.getDamageListId();
//        List<String> list = Arrays.asList(damageListGoodsStr);
//        List<DamageListGoods> damageListGoodsList = list.stream().map(item -> {
//            DamageListGoods damageListGoods = JSONs.toJSONObj(item,DamageListGoods.class);
//            damageListGoods.setDamageListId(damageListId);
//            return damageListGoods;
//        }).collect(Collectors.toList());
//        List<DamageListGoods> damageListGoodsList = new ArrayList<>();
//        for (String s : list) {
//            DamageListGoods damageListGoods = JSONs.toJSONObj(s,DamageListGoods.class);
//            damageListGoods.setDamageListId(damageListId);
//            damageListGoodsList.add(damageListGoods);
//        }
        List<DamageListGoods> damageListGoodsList = JSONs.jsonStrToObjMax(damageListGoodsStr, new TypeReference<List<DamageListGoods>>() {
        });
        damageListGoodsList.stream().forEach(item->{
            item.setDamageListId(damageListId);
            kcDao.saveDamageListGoods(item);
        });
    }

    @Override
    public void saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr,User user) {
        overflowList.setUserId(user.getUserId());
        overflowList.setTrueName(user.getTrueName());
        kcDao.saveOverflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        List<OverflowListGoods> overflowListGoods = JSONs.jsonStrToObjMax(overflowListGoodsStr, new TypeReference<List<OverflowListGoods>>() {
        });
        overflowListGoods.stream().forEach(item->{
            item.setOverflowListId(overflowListId);
            kcDao.saveOverflowListGoods(item);
        });
    }

    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> list = kcDao.listAlarm();
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> damageList(String  sTime,String  eTime) {
        List<DamageList> list = kcDao.damageList(sTime,eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> overflowList(String sTime, String eTime) {
        List<OverflowList> list = kcDao.overflowList(sTime,eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> damageListGoods(Integer damageListId) {
        List<DamageListGoods> list = kcDao.damageListGoods(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> overflowListGoods(Integer overflowListId) {
        List<OverflowListGoods> list = kcDao.overflowListGoods(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
