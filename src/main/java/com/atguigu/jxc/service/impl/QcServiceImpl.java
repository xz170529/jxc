package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.QcDao;
import com.atguigu.jxc.dao.SpDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.QcService;
import com.atguigu.jxc.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * time:  2023/9/11
 */
@Service
public class QcServiceImpl implements QcService {
    @Autowired
    QcDao qcDao;
    @Autowired
    GoodsDao goodsDao;
    @Autowired
    SpDao spDao;
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> list = qcDao.getNoInventoryQuantity(nameOrCode);
        list.stream().forEach(goods->{
            Integer typeId = goods.getGoodsTypeId();
            String gt = goodsDao.getGt(typeId);
            goods.setGoodsTypeName(gt);
        });
        int total = list.size();
        List<Goods> pageList = PageUtil.getPageList(list, page, rows);
        map.put("total",total);
        map.put("rows",pageList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> list = qcDao.getHasInventoryQuantity(nameOrCode);
        list.stream().forEach(goods->{
            Integer typeId = goods.getGoodsTypeId();
            String gt = goodsDao.getGt(typeId);
            goods.setGoodsTypeName(gt);
        });
        int total = list.size();
        List<Goods> pageList = PageUtil.getPageList(list, page, rows);
        map.put("total",total);
        map.put("rows",pageList);
        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        qcDao.update(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public Integer deleteStock(Integer goodsId) {
        Goods goods = spDao.selectOne(goodsId);
        if(goods.getState() == 0){
            qcDao.deleteStock(goodsId);
            return 0;
        }if(goods.getState() == 1){
            return 1;
        }else {
            return 2;
        }
    }
}
