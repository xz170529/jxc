package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;

import java.util.Map;

/**
 * time:  2023/9/11
 */
public interface KcService {
    void saveDamageListGoods(DamageList damageList, String damageListGoodsStr,User user);

    void saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr,User user);

    Map<String, Object> listAlarm();

    Map<String, Object> damageList(String  sTime,String  eTime);

    Map<String, Object> overflowList(String sTime, String eTime);

    Map<String, Object> damageListGoods(Integer damageListId);

    Map<String, Object> overflowListGoods(Integer overflowListId);
}
