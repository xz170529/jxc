package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * time:  2023/9/11
 */
public interface KhService {
    Map<String, Object> getKhList(Integer page, Integer rows, String customerName);

    void save(Customer customer);

    void delete(String ids);
}
