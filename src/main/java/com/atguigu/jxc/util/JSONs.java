package com.atguigu.jxc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

/**
 * time:  2023/8/21
 */
@Slf4j
public class JSONs {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    public static String toJSONStr(Object obj){
        String string =null;
        try {
            string = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("json转化错误：{}",e);
        }
        return string;
    }
    public static<T> T toJSONObj(String s,Class<T> obj){
        T t = null;
        try {
            t = objectMapper.readValue(s, obj);
        } catch (JsonProcessingException e) {
            log.info("字符转换异常");
        }
        return t;
    }
    /**
     * 复杂类型转换
     * @param json
     * @param ref
     * @return
     * @param <T>
     */
    public static<T> T jsonStrToObjMax(String json, TypeReference<T> ref){
        T t = null;
        try {
            t = objectMapper.readValue(json, ref);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return t;
    }
}
