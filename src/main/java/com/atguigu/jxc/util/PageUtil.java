package com.atguigu.jxc.util;

import java.util.ArrayList;
import java.util.List;

/**
 * time:  2023/9/11
 */
public class PageUtil {
    public static <T> List<T> getPageList(List<T> list,Integer page, Integer rows){
        int total = list.size();
        int pageAll = 0;
        if(total%rows == 0 ){
            pageAll = total/rows;
        }else {
            pageAll = total/rows+1;
        }
        ArrayList<T> pageList = new ArrayList<>();

        //分页list开始位置
        int start = (page - 1) * rows;
        //分页list结束位置
        int end = 0;
        //判断是否是最后一页
        if(page*rows>total){
            end = total;
        }else {
            end = start+rows;
        }
        //实现分页数据
        for (int i = start; i < end; i++) {
            pageList.add(list.get(i));
        }
        return pageList;
    }
}
